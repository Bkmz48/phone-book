<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class ApiController extends Controller {

  public function getUsers() {
    $users = User::all();

    return response()->json([
      'users' => $users,
    ], 200);
  }

  public function search($value) {
    $user = User::where('name', $value)->first();

    if (!$user) {
      return response()->json([
        'message' => 'Пользователь не найден в базе',
        'errors' => ['user' => 'not found'],
      ], 404);
    }

    return response()->json([
      'user' => $user,
    ], 200);
  }

  public function store(Request $request) {
    if ($request->has('user')) {
      $user_data = json_decode($request['user']);

      $user = User::make();
      $user->name = $user_data->name;
      $user->phone = $user_data->phone;
      $user->save();

      return response()->json([
        'user' => $user,
      ], 200);
    }
  }

  public function update(Request $request, User $user) {
    if (!$user) {
      return response()->json([
        'message' => 'Пользователь не найден в базе',
        'errors' => ['user' => 'not found'],
      ], 404);
    }

    if ($request->has('user')) {
      $user_data = json_decode($request['user']);

      $user->name = $user_data->name;
      $user->phone = $user_data->phone;
      $user->save();

      return response()->json([
        'user' => $user,
      ], 200);
    }
  }

  public function delete(User $user) {
    if (!$user) {
      return response()->json([
        'message' => 'Пользователь не найден в базе',
        'errors' => ['user' => 'not found'],
      ], 404);
    }

    $user->delete();

    return response()->json([
      'delete' => true,
    ], 200);
  }
}