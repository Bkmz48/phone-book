<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <title>Телефонная книга</title>
</head>
<body>
<div class="container">
    <h1 class="my-4">Телефонная книга</h1>
    <hr>
    <div>
        <a href="{{route('users.get')}}">Все пользователи</a>
    </div>
    <hr>
    <div>
        <h3>Добавление</h3>
        <form id="user-store"
              method="POST"
              action="{{route('users.store')}}">
            @csrf
            <textarea class="form-control" rows="3" name="user"></textarea>
            <small class="form-text text-muted">
                Json строка, формат { "name": "", "phone": "" }
            </small>
            <br>
            <button type="submit" class="btn btn-info">Сохранить</button>
        </form>
    </div>
    <hr>
    <div>
        <h3>Обновление</h3>
        <form id="user-update"
              method="POST"
              action="#">
            @method('PUT')
            @csrf
            <input type="number" id="user-id-update" name="user-id" class="form-control mb-4" placeholder="id пользователя для обновления">
            <textarea class="form-control" rows="3" name="user"></textarea>
            <small class="form-text text-muted">
                Json строка, формат { "name": "", "phone": "" }
            </small>
            <br>
            <button type="submit" class="btn btn-info">Сохранить</button>
        </form>
    </div>
    <hr>
    <div>
        <h3>Удаление</h3>
        <form id="user-delete"
              method="POST"
              action="#">
            @method('DELETE')
            @csrf
            <input type="number" id="user-id-delete" name="user-id" class="form-control" placeholder="id пользователя для удаления">
            <br>
            <button type="submit" class="btn btn-info">Удалить</button>
        </form>
    </div>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
