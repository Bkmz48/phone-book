/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

if (document.getElementById('user-update')) {
  let form = document.getElementById('user-update');

  form.addEventListener('submit', function (event) {
    let id = document.getElementById('user-id-update').value;
    form.action = 'api/users/' + id;
    form.submit();

  }, true);

}

if (document.getElementById('user-delete')) {
  let form = document.getElementById('user-delete');

  form.addEventListener('submit', function (event) {
    let id = document.getElementById('user-id-delete').value;
    form.action = 'api/users/' + id;
    form.submit();

  }, true);

}

