<?php


Route::get('/', function () {
  return view('welcome');
});

Route::prefix('api')->group(function () {
  Route::get('users', 'ApiController@getUsers')->name('users.get');
  Route::get('users/search/{name}', 'ApiController@search')->name('users.search');
  Route::post('users', 'ApiController@store')->name('users.store');
  Route::put('users/{user}', 'ApiController@update')->name('users.update');
  Route::delete('users/{user}', 'ApiController@delete')->name('users.delete');
});